#include <iostream>

using namespace std;

class Optim
{
    public :
        static double getPrecision()
        {
            return 1e-15;
        }
        static double Abs(double number)
        {
            return ( number >= 0 ? number : -1 * number);
        }
        static double pow(double number, int n)
        {
            double res = 1 ;
            for(int i = 0 ; i < n ; i++)
            {
                res = res * number;
            }
            return res;
        }
        static double GetNthRoot(double number, int n)
        {
            double high = number;
            double low = 1;
            double mid = (high + low ) / 2;
            double powRes = pow(mid, n);

            while(Abs(powRes- number) > getPrecision() && (high - low) > getPrecision())
            {
                if(powRes > number )
                {
                    high = mid;
                }
                else
                {
                    low = mid;
                }
                mid = (high + low ) / 2;
                powRes = pow(mid, n);
            }
            return mid;
        }

};
int main()
{
    Optim O;
    cout.precision(15);
    cout << O.GetNthRoot(2,2) << endl;
    cout << O.GetNthRoot(10,7) << endl;
    cout << O.GetNthRoot(27,3) << endl;

    return 0;
}
