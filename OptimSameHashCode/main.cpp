#include <iostream>
#include <functional>
#include <map>

using namespace std;
class OptimSameHash
{
    public :
        static char getMaxChar()
        {
            return '~';
        }
        static char getMinChar()
        {
            return ' ';
        }
        static string genNextString(string s)
        {
            string res ="";

            if(s.size() == 0 || s == string(s.size(),getMaxChar()))
            {
                res = string(s.size() +1, getMinChar());
            }
            else
            {
                res = s;
                for(int i = res.size() -1 ; i >= 0 ;i--)
                {
                    if(res[i] == getMaxChar())
                    {
                        res[i] = getMinChar();
                    }
                    else
                    {
                        res[i] = (char) (res[i] + 1);
                        break;
                    }
                }
            }
            return res;
        }
        static void GenSameHash()
        {
            std::hash<std::string> str_hash;
            string s = "" ;
            size_t fin = 2 * 1e8;
            map<size_t,short> m;
            bool bFound = false;
            size_t FHash ;

            for(size_t i = 0; i<= fin ;i++)
            {
                s = genNextString(s);
                size_t hsh = str_hash(s);
                m[hsh] = m[hsh] +1 ;
                if(m[hsh] >= 3)
                {
                    bFound = true ;
                    FHash = hsh;
                    break;
                }
            }
            m.clear();
            if(bFound)
            {
                s = "" ;
                cout<<"Strings with same hash:"<<endl;
                for(size_t i = 0; i <= fin ;i++)
                {
                    s = genNextString(s);
                    size_t hsh = str_hash(s);
                    if(hsh == FHash)
                    {
                        cout<<s<<endl;
                    }
                }
            }
            else
            {
                cout<<"Not Found"<<endl;
            }
        }
};
int main()
{
    OptimSameHash obj;
    obj.GenSameHash();
    return 0;
}
